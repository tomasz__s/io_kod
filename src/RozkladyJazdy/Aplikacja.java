/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RozkladyJazdy;

import java.util.*;
import java.time.*;
import RozkladyJazdy.Model.Komunikat;
import RozkladyJazdy.Model.Linia;
import RozkladyJazdy.Model.Przystanek;


/**
 *
 * @author piotr
 */
public class Aplikacja {
    private ArrayList<Linia> linie = new ArrayList<Linia>();
    private ArrayList<Przystanek> przystanki = new ArrayList<Przystanek>();
    private UslugaAutoryzacji uslugaAutoryzacji = new UslugaAutoryzacji();
    private ArrayList<Komunikat> komunikaty = new ArrayList<Komunikat>();
    
   boolean szukajPrzystanek(String nazwa){
       Iterator it = przystanki.iterator();
       
       while(it.hasNext())
       {
           Przystanek przystanek = (Przystanek)it.next();
           String nazwaPrzystanku = przystanek.pobierzNazwe();
           if(nazwa.equalsIgnoreCase(nazwaPrzystanku) == true)
           {
               return true;
           }
       }
       return false;
   }
   
    boolean szukajLinie(String nazwa){
       Iterator it = przystanki.iterator();
       
       while(it.hasNext())
       {
           Linia linia = (Linia)it.next();
           String nazwaLinii = linia.pobierzNazwe();
           if(nazwa.equalsIgnoreCase(nazwaLinii) == true)
           {
               return true;
           }
       }
       return false;
   }
    
   void dodajPrzystanek(String nazwaPrzystanku)
   {
       Przystanek przystanek = new Przystanek();
       if(szukajPrzystanek(nazwaPrzystanku) == false)
       {
           przystanek.ustawNazwe(nazwaPrzystanku);
           przystanki.add(przystanek);
       }
       
   }
   
   void dodajLinie(String nazwaLinii)
   {
       Linia linia = new Linia();
       if(szukajPrzystanek(nazwaLinii) == false)
       {
           linia.ustawNazwe(nazwaLinii);
           linie.add(linia);
       }
       
   }
   
   void dodajKomunikat(String trescKomunikatu)
   {
       Komunikat komunikat = new Komunikat();
       if(trescKomunikatu!=null && !trescKomunikatu.isEmpty())
       {
           komunikat.ustawTresc(trescKomunikatu);
           LocalDateTime currentDate = LocalDateTime.now();
           komunikat.ustawDate(currentDate);
           komunikaty.add(komunikat);
       }
   }
   
   void wyswietlWszystkiePrzystanki()
   {
       Iterator it = przystanki.iterator();
       while(it.hasNext())
       {
           Przystanek przystanek = (Przystanek)it.next();
           System.out.println(przystanek.pobierzNazwe());
       }
   }
   
      void wyswietlWszystkieLinie()
   {
       Iterator it = linie.iterator();
       while(it.hasNext())
       {
           Linia linia = (Linia)it.next();
           System.out.println(linia.pobierzNazwe());
       }
   }
    
}
