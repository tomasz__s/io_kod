/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RozkladyJazdy.Model;

import java.time.LocalDateTime;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

/**
 *
 * @author piotr
 */
public class Komunikat {
    private String tresc;
    private LocalDateTime data;
    
    public Komunikat(){
        
    }
    
    public String pobierzTresc(){
        return tresc;
    }
    public void ustawTresc(String tresc){
        this.tresc = tresc;
    }
    public LocalDateTime pobierzDate(){
        return data;
    }
    public void ustawDate(LocalDateTime data){
        this.data = data;
    }
}
